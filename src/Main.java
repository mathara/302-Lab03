import com.Matheus.base.*;
import com.Matheus.util.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//lacaio deckBaralho
		CartaLacaio lac1 = new CartaLacaio(1, "Frodo Bolseiro",2,1,1);
		CartaLacaio lac2 = new CartaLacaio(2,"Aragorn", 5, 7, 6);
		CartaLacaio lac3 = new CartaLacaio(3, "Legolas", 8, 4, 6);
		
		//baralho
		Baralho deckB = new Baralho();
		
		//addicionar cartas ao Baralho
		deckB.adicionarCarta(lac1);
		deckB.adicionarCarta(lac2);
		deckB.adicionarCarta(lac3);
		
		//embaralhar
		System.out.println("Baralho normal embaralhado");
		deckB.embaralhar();
		
		//lacaioBaralhoArray
		CartaLacaio lac4 = new CartaLacaio(4, "Druida da Noite", 7, 8, 6);
		CartaLacaio lac5 = new CartaLacaio(5, "Cientista Maluco", 3, 5, 4);
		CartaLacaio lac6 = new CartaLacaio(6, "Jacicoê,Marty", 6, 4, 5);
		
		//baralhoArray
		BaralhoArrayList deckBA = new BaralhoArrayList();
		
		//addicionar cartas ao Baralho
		deckBA.adicionarCarta(lac4);
		deckBA.adicionarCarta(lac5);
		deckBA.adicionarCarta(lac6);		
		
		//embaralhar
		System.out.println("Baralho Array embaralhado");
		deckBA.embaralhar();
		
		//lacaio add-extra
		CartaLacaio lac7 = new CartaLacaio(7, "Leroy Jekins", 6, 2, 5);
		CartaLacaio lacx  = new CartaLacaio(10, "Logan", 10, 10, 10);
		
		//compra de cartas
		System.out.println("\nCarta Comprada");
		System.out.println(deckBA.comprarCarta());
		
		//ganho de cartas buffadas
		//buffar
		Util.buffar(lac7,7);
		System.out.println(lac7);
		Util.buffar(lacx,3,4);
		System.out.println(lacx);
		//add
		deckB.adicionarCarta(lac7);
		deckBA.adicionarCarta(lacx);
		
		//impressão do baralho
		System.out.println("Baralho");
		deckB.imprimirBaralho();
		//impressão do baralhoArray
		System.out.println("Baralho Array");
		deckBA.imprimirBaralho();
		
	}
}
