package com.Matheus.base;

import java.util.ArrayList;
import java.util.Collections;
import com.Matheus.util.Util;



public class BaralhoArrayList {
	private ArrayList<CartaLacaio> vetorCartas;
	
	public BaralhoArrayList(){
		vetorCartas = new ArrayList<CartaLacaio>();
	}
	
	public void adicionarCarta (CartaLacaio card){
		if (vetorCartas.size() <=  Util.MAX_CARDS){
			vetorCartas.add(card);
			// não preciso de uma váriavel que contenha o tamanho
		}
	}
	
	public CartaLacaio comprarCarta(){
		CartaLacaio aux = vetorCartas.get(vetorCartas.size()-1);	//pega o lacaio do topo da pilha
		vetorCartas.remove(vetorCartas.size()-1);					//remove da pilha
		return aux;													//Lacaio do topo da pilha
	}
	
	public void embaralhar(){
		Collections.shuffle(vetorCartas);
		ArrayList<CartaLacaio> baux = vetorCartas;
		Collections.reverse(baux);
		System.out.println(baux);
	}
	
	public void imprimirBaralho(){
		System.out.println(vetorCartas);
	}
}
