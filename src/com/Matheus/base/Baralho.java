package com.Matheus.base;

import java.util.Random;

public class Baralho {
	CartaLacaio[] vetorCartas;
	int nCartas;
	static Random gerador = new Random();
	
	public Baralho(){
		vetorCartas = new CartaLacaio[10];
		nCartas = 0;
	}
	
	public void adicionarCarta (CartaLacaio card){
		vetorCartas [nCartas] = card; //adiciona carta no baralho no final
		nCartas++;					  // atualiza a posição
	}
	
	public CartaLacaio comprarCarta(){
		nCartas--;						//tira a carta do baralho
		return vetorCartas[nCartas];	//carta que vai para a mão do jogador
	}
	
	public void embaralhar(){
		int i,j;
		
		for (i = 1; i < nCartas; i++){
			j = gerador.nextInt(i+1);
			System.out.println("\ngerador" + j+"\n");
			if( j != i ){
				CartaLacaio a = vetorCartas[i];
				CartaLacaio b = vetorCartas[j];
				vetorCartas[i] = b;
				vetorCartas[j] = a;				
			}
		}
		//alteração do código criando uma função de impressão do baralho
		imprimirBaralho();
		
	}
	
	public void imprimirBaralho(){
		int i;
		
		for (i = nCartas-1; i >= 0; i--){
			CartaLacaio a = vetorCartas[i]; // seleciona o lacaio
			System.out.println(a);
		}
	}
}
