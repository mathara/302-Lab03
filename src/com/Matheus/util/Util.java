package com.Matheus.util;

import com.Matheus.base.CartaLacaio;

public class Util {
	public static final int MAX_CARDS = 30;
	
	public static void buffar(CartaLacaio lac,int a){
		if (a > 0){
			lac.setVidaAtual(lac.getVidaAtual() + a);
			lac.setVidaMaxima(lac.getVidaMaxima() + a);
			lac.setAtaque(lac.getAtaque() + a);
			alteraNomeFortalecido(lac);
		}
	}
	
	public static void buffar(CartaLacaio lac, int a, int v){
		if (a > 0 && v > 0){
			lac.setVidaAtual(lac.getVidaAtual() + v);
			lac.setVidaMaxima(lac.getVidaMaxima() + v);
			lac.setAtaque(lac.getAtaque() + a);
			alteraNomeFortalecido(lac);
		}
	}
	
	public static void alteraNomeFortalecido(CartaLacaio lac){
		lac.setNome( lac.getNome()+ "Buffed");
	}

}
